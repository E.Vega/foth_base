import {Request, Response} from 'express';
import pool from '../database/database';
import { promises } from 'fs';
import {} from 'jsonwebtoken';

class UserCOntroller{

    public async listUsers(req: Request, res: Response){
        const users = await pool.query('select * from user');
        if(users.length >0 ){
            return res.json(users);
        }else{
            return res.status(404).json({text: 'no hay usuarios registrados'});
        }
    }
    public async selectUser(req: Request, res: Response){
        const { id } = req.params;
        const user = await pool.query('select * from user where id = ?',[id]);
        if(user.length > 0){
            return res.json(user[0]);
        }else{
            return res.status(404).json({text: 'id no existe'});
        }
    }
    public async createUser(req: Request, res: Response){
        var name = req.param("name");
        var last_name = req.param("last_name");
        var user_name = req.param("user_name");
        var email = req.param("email");
        var phone_number = req.param("phone_number");
        var password = req.param("password");
        await pool.query('INSERT INTO user (name,last_name,user_name,email,phone_number,password) values (?,?,?,?,?,MD5(?))',[name,last_name,user_name,email,phone_number,password]);
        res.json({text: 'Usuario Guardado'});
    }
    public async delete(req: Request, res: Response){
        const { id } = req.params;
        await pool.query('delete from user where id = ?', [id]);
        res.json({text: 'usuario eliminado'})
    }
    public async update(req: Request, res: Response){
        const { id } = req.params;
        await pool.query('update user set ? where id = ?',[req.body, id]);
        res.json({text:'usuario modificado'});
    }
    login(req: Request, res: Response){
        const userName = req.param('name');
        const userPass = req.param('password');
    }
}
export const userController = new UserCOntroller();