import { Router } from 'express';
import { userController } from '../controllers/userController';

class UserRoutes{

    router: Router = Router();

    constructor(){
        this.config();
    }

    config(): void{
        this.router.get("/",userController.listUsers);
        this.router.get("/:id",userController.selectUser);
        this.router.post("/",userController.createUser);
        this.router.delete("/:id",userController.delete);
        this.router.put("/:id",userController.update);
    }
    
}

const userRoutes = new UserRoutes();
export default userRoutes.router;