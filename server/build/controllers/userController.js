"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database/database"));
class UserCOntroller {
    listUsers(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield database_1.default.query('select * from user');
            if (users.length > 0) {
                return res.json(users);
            }
            else {
                return res.status(404).json({ text: 'no hay usuarios registrados' });
            }
        });
    }
    selectUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const user = yield database_1.default.query('select * from user where id = ?', [id]);
            if (user.length > 0) {
                return res.json(user[0]);
            }
            else {
                return res.status(404).json({ text: 'id no existe' });
            }
        });
    }
    createUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            var name = req.param("name");
            var last_name = req.param("last_name");
            var user_name = req.param("user_name");
            var email = req.param("email");
            var phone_number = req.param("phone_number");
            var password = req.param("password");
            yield database_1.default.query('INSERT INTO user (name,last_name,user_name,email,phone_number,password) values (?,?,?,?,?,MD5(?))', [name, last_name, user_name, email, phone_number, password]);
            res.json({ text: 'Usuario Guardado' });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('delete from user where id = ?', [id]);
            res.json({ text: 'usuario eliminado' });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('update user set ? where id = ?', [req.body, id]);
            res.json({ text: 'usuario modificado' });
        });
    }
    login(req, res) {
        const userName = req.param('name');
        const userPass = req.param('password');
    }
}
exports.userController = new UserCOntroller();
