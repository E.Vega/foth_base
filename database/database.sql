SET NAMES utf8;

SET SQL_MODE='';

create database if not exists foth_db;

USE foth_db;

DROP TABLE IF EXISTS user;

CREATE TABLE user (
  id int(11) not null auto_increment primary key,
  name varchar(50),
  last_name varchar(200),
  user_name varchar(200),
  email varchar(60),
  phone_number int(20),
  password varchar(200)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;